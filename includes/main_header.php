<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet" />

  <title>IVATLE</title>
  <!--
Elegance Template
https://templatemo.com/tm-528-elegance
-->
  <!-- Additional CSS Files -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

  <link rel="stylesheet" type="text/css" href="css/font-awesome.css" />

  <link rel="stylesheet" type="text/css" href="css/fullpage.min.css" />

  <link rel="stylesheet" type="text/css" href="css/owl.carousel.css" />

  <link rel="stylesheet" href="css/animate.css" />

  <link rel="stylesheet" href="css/templatemo-style.css" />

  <link rel="stylesheet" href="css/responsive.css" />
  <link rel="stylesheet" href="css/style.css" />
</head>

<body>
  <div id="video" class="banner">
    <div class="preloader">
      <div class="preloader-bounce">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>

    <header id="header">
      <div class="container-fluid">
        <div class="navbar">
          <a href="index.php#Home" id="logo" title="IVATLE"> IVATLE </a>
          <div class="navigation-row">
            <nav id="navigation">
              <button type="button" class="navbar-toggle">
                <i class="fa fa-bars"></i>
              </button>
              <div class="nav-box navbar-collapse">
                <ul class="navigation-menu nav navbar-nav navbars" id="nav">
                  <li data-menuanchor="Home" class="active">
                    <a href="index.php#Home">Home</a>
                  </li>
                  <li data-menuanchor="AboutUs">
                    <a href="index.php#AboutUs">About Us</a>
                  </li>
                  <li data-menuanchor="Services">
                    <a href="index.php#Services">Services</a>
                  </li>
                  <li data-menuanchor="WorkWithUs">
                    <a href="index.php#WorkWithUs">Work With Us</a>
                  </li>
                  <li data-menuanchor="ContactUs">
                    <a href="index.php#ContactUs">Contact Us</a>
                  </li>
                  <li data-menuanchor="Privacypolicy">
                    <a href="privacy-policy.php#Privacypolicy">Privacy Policy</a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header>