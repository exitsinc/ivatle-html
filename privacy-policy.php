<?php include('includes/main_header.php'); ?>

        <div id="fullpage" class="fullpage-default">

            <Section>
                <div class="section animated-row" data-section="PrivacyPolicy">
                    <div class="section-inner">
                        <div class="container">
                            <div class="row">
                                <div class="privacy_text">
                                    <h1>Privacy Policy - Ivatle</h1>
                                    <h3>Introduction</h3>
                                    <p>Ivatle Private Limited (hereinafter “Ivatle”) recognizes the importance of privacy
                                        of its users and also of maintaining confidentiality of the information provided
                                        by its users as a responsible data controller and data processer.</p>
                                    <p>This Privacy Policy provides for the practices for handling and securing user's
                                        Personal Information (defined hereunder) by Ivatle and its subsidiaries and
                                        affiliates.</p>
                                    <p>This Privacy Policy is applicable to any person (‘User’) who purchase, intend to
                                        purchase, or inquire about any product(s) or service(s) made available by Ivatle
                                        through any of Ivatle’s customer interface channels including its website, mobile
                                        site, mobile app &amp; offline channels including call centers and offices
                                        (collectively referred herein as "Sales Channels").</p>
                                    <p>For the purpose of this Privacy Policy, wherever the context so requires "you" or
                                        "your" shall mean User and the term "we", "us", "our" shall mean Ivatle. For the
                                        purpose of this Privacy Policy, Website means the website(s), mobile site(s) and
                                        mobile app(s).</p>
                                    <p>By using or accessing the Website or other Sales Channels, the User hereby agrees
                                        with the terms of this Privacy Policy and the contents herein. If you disagree
                                        with this Privacy Policy please do not use or access our Website or other Sales
                                        Channels.</p>
                                    <p>This Privacy Policy does not apply to any website(s), mobile sites and mobile
                                        apps of third parties, even if their websites/products are linked to our
                                        Website. User should take note that information and privacy practices of Ivatle’s
                                        business partners, advertisers, sponsors or other sites to which Ivatle provides
                                        hyperlink(s), may be materially different from this Privacy Policy. Accordingly,
                                        it is recommended that you review the privacy statements and policies of any
                                        such third parties with whom they interact.</p>
                                    <p>This Privacy Policy is an integral part of your User Agreement with Ivatle and all
                                        capitalized terms used, but not otherwise defined herein, shall have the
                                        respective meanings as ascribed to them in the User Agreement.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="privacy_text">
                                    <h2>USERS OUTSIDE THE GEOGRAPHICAL LIMITS OF INDIA</h2>
                                    <hr>
                                    <p>Please note that the data shared with Ivatle shall be primarily processed in India
                                        and such other
                                        jurisdictions where a third party engaged by Ivatle may process the data on Ivatle’s
                                        behalf. By
                                        agreeing to this policy, you are providing Ivatle with your explicit consent to
                                        process your
                                        personal information for the purpose(s) defined in this policy. The data
                                        protection regulations
                                        in India or such other jurisdictions mentioned above may differ from those of
                                        your country of
                                        residence.</p>
                                    <p>If you have any concerns in the processing your data and wish to withdraw your
                                        consent, you may
                                        do so by writing to the following email id: privacy@go-Ivatle.com. However, if such
                                        processing of
                                        data is essential for us to be able to provide service to you, then we may not
                                        be able to serve
                                        or confirm your bookings after your withdrawal of consent. For instance, if you
                                        want to book any
                                        international holiday package in fixed departures (group bookings), then certain
                                        personal
                                        information of yours like contact details, gender, dietary preferences, choice
                                        of room with
                                        smoking facility, any medical condition which may require specific attention or
                                        facility etc.
                                        may have to be shared by us with our vendors in each city where you will stay,
                                        and they may
                                        further process this information for making suitable arrangements for you during
                                        the holiday.
                                        Such sharing and processing of information may extend to the hotel where you
                                        will stay or the
                                        tour manager who will be your guide during the travel. </p>
                                    <p>A withdrawal of consent by you for us to process your information may:<ul
                                            style="padding: 0px 30px;">
                                            <li>Severely inhibit our ability to serve you properly and in such case, we
                                                may have to
                                                refuse the booking altogether, or</li>
                                            <li>Unreasonably restrict us to service your booking (if a booking is
                                                already made) which
                                                may further affect your trip or may compel us to cancel your booking.
                                            </li>
                                        </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                            </div>
                        </div>
                    </div>
                </div>
            </Section>
        </div>
<?php include('includes/main_footer.php'); ?>
       