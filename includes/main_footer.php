 <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/fullpage.min.js"></script>

    <script src="js/scrolloverflow.js"></script>

    <script src="js/owl.carousel.min.js"></script>

    <script src="js/jquery.inview.min.js"></script>

    <script src="js/form.js"></script>

    <script src="js/custom.js"></script>
    <script>
      $(document).ready(function () {
        $('#submit_form').on('click', function (e) {
          e.preventDefault();
          $.ajax({
            type: "POST",
            url: "https://admin.ivatle.com/api/user/apicontactform",
            data: {
              name: $('#name').val(),
              email: $('#email').val(),
              message: $('#message').val(),
            },
            success: function (response) {
              console.log(response, {
                name: $('#name').val(),
                email: $('#email').val(),
                message: $('#message').val(),
              });
              $('#name').val()
              $('#email').val()
              $('#message').val();
              location.reload();
            }
          })
        });

      });
    </script>
</body>

</html>