<?php include('includes/main_header.php'); ?>
    <div id="fullpage" class="fullpage-default">
      <div class="section animated-row" data-section="Home">
        <div class="section-inner">
          <div class="welcome-box">
            <span class="welcome-first animate" data-animate="fadeInUp">Hello, welcome to</span>
            <h1 class="welcome-title animate" data-animate="fadeInUp">
              IVATLE
            </h1>
            <!-- <p class="animate" data-animate="fadeInUp">This is a clean and modern HTML5 template with a video background. You can use this layout for your profile page. Please spread a word about templatemo to your friends. Thank you.</p> -->
            <p>
              The company provides platform to plan personalised luxury trips, within drivable distance of the city.
            </p>
            <div class="scroll-down next-section animate
              data-animate=" fadeInUp"">
              <img src="images/mouse-scroll.png" alt="" /><span>Scroll Down</span>
            </div>
          </div>
        </div>
      </div>
      <div class="section animated-row" data-section="About">
        <div class="section-inner">
          <div class="about-section">
            <div class="row justify-content-center">
              <div class="col-lg-8 wide-col-laptop">
                <div class="row">
                  <div class="col-md-6">
                    <div class="about-contentbox" style="margin-top: 7%">
                      <div class="animate" data-animate="fadeInUp">
                        <span>About Us</span>
                        <h2>Who We Are?</h2>
                        <!-- <p>Credits go to <strong>Unsplash</strong> and <strong>Pexels</strong> for photos and video used in this template. Vivamus tincidunt, augue rutrum convallis volutpat, massa lacus tempus leo.</p> -->
                        <p>
                          Term <strong>Ivatle</strong> originates from the Sanskrit word <strong>ईवत्</strong> which means <strong>'so magnificent'</strong>. Our aim is to give a magnificent travel experiences for local travellers, with hand picked and personalised places to stay and visit. 
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <figure class="about-img animate" data-animate="fadeInUp">
                      <img src="images/lonavla1.jpg" class="rounded" alt="" />
                    </figure>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section animated-row" data-section="Services">
        <div class="section-inner">
          <div class="about-section">
            <div class="row justify-content-center">
              <div class="col-lg-8 wide-col-laptop">
                <div class="row">
                  <div class="col-md-6">
                    <figure class="about-img animate" data-animate="fadeInUp">
                      <img src="images/services.jpg" class="rounded" alt="" />
                    </figure>
                  </div>
                  <div class="col-md-6">
                    <div class="about-contentbox" style="margin-top: 5%">
                      <div class="animate" data-animate="fadeInUp">
                        <span>Services</span>
                        <h2>What We Do?</h2>
                        <p>
                          <strong>Listing Platform:</strong><br>
                          We provide listing platform for small and medium scale travel agents and hosts, so that they can connect with the right customers for their services.
                        </p>
                        <p><strong>Customized Platform:</strong><br>
                          We provide customized travel planner for travellers who prefer luxury travel within drivable distance, to keep themselves safe in Covid situations.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section animated-row" data-section="WorkWithUs">
        <div class="section-inner">
          <div class="row justify-content-center">
            <div class="col-md-8 wide-col-laptop">
              <div class="title-block animate" data-animate="fadeInUp">
                <span>Work With Us</span>
                <h2>We Help With</h2>
              </div>
              <div class="services-section">
                <div class="services-list owl-carousel">
                  <div class="item animate" data-animate="fadeInUp">
                    <div class="service-box">
                      <span class="service-icon"><i class="fa fa-car" aria-hidden="true"></i></span>
                      <h3>Safe Travel</h3>
                      <p>
                        Providing safe and luxurious travel destinations, within drivable distance
                      </p>
                    </div>
                  </div>
                  <div class="item animate" data-animate="fadeInUp">
                    <div class="service-box">
                      <span class="service-icon"><i class="fa fa-comments" aria-hidden="true"></i></span>
                      <h3>Geniune Clients</h3>
                      <p>
                        Reaching out to the most appropriate clients interested in luxury travel
                      </p>
                    </div>
                  </div>
                  <div class="item animate" data-animate="fadeInUp">
                    <div class="service-box">
                      <span class="service-icon"><i class="fa fa-users" aria-hidden="true"></i></span>
                      <h3>Ease of Access</h3>
                      <p>
                        Onboard Travel Vendor's data, so that they can interact better with their customers
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      <!-- <div class="section animated-row" data-section="slide06">
<div class="section-inner">
                    <div class="row justify-content-center">
                        <div class="col-md-7 wide-col-laptop">
                            <div class="title-block animate" data-animate="fadeInUp">
                                <span>Contact Us</span>
                                <h2>Get In Touch!</h2>
                            </div>
                            <div class="contact-section">
                                <div class="row">
                                    <div class="col-md-6 animate" data-animate="fadeInUp">
                                        <div class="contact-box">
                                            <div class="contact-row">
                                                <i class="fa fa-map-marker"></i><span style="margin-left: -10px"> 809, Appaji mate chowk, tulsi bagh road, mahal, nagpur, 440032, India</span>
                                            </div>
                                            <div class="contact-row">
                                                <i class="fa fa-phone"></i><span style="margin-left: -10px">+919822936696</span>
                                                <i class="fa fa-phone" style="margin-left: 20px"></i><span style="margin-left: -10px">8454965547</span> 
                                            </div>
                                            <div class="contact-row">
                                                <i class="fa fa-envelope"></i> <span style="margin-left: -10px">piyushpy@ivatle.com</span>
                                                <i class="fa fa-envelope" style="margin-left: 20px"></i><span style="margin-left: -10px">info@exits.in</span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6 animate" data-animate="fadeInUp">
                                        <form id="ajax-contact" method="post" action="#">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="name" id="name" required placeholder="Name">
                                            </div>
                                            <div class="input-field">
                                                <input type="email" class="form-control" name="email" id="email" required placeholder="Email">
                                            </div>
                                            <div class="input-field">
                                                <textarea class="form-control" name="message" id="message" required placeholder="Message"></textarea>
                                            </div>
                                            <button class="btn" type="submit">Submit</button>
                                        </form>
                                        <div id="form-messages" class="mt-3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
      <div class="section animated-row" data-section="Contact Us">
        <div class="section-inner">
          <div class="row justify-content-center">
            <div class="col-md-7 wide-col-laptop">
              <div class="title-block animate" data-animate="fadeInUp">
                <span>Contact Us</span>
                <h2>Get In Touch!</h2>
              </div>
              <div class="contact-section">
                <div class="row">
                  <div class="col-md-6 animate" data-animate="fadeInUp">
                    <div class="contact-box">
                      <div class="contact-row">
                        <i class="fa fa-map-marker"></i><span style="margin-left: -10px">
                          809, Appaji mate chowk, tulsi bagh road, mahal,
                          nagpur, 440032, India</span>
                      </div>
                      <div class="contact-row">
                        <i class="fa fa-phone"></i><span style="margin-left: -10px">+447438298018</span>
                        <!-- <i class="fa fa-phone" style="margin-left: 20px"> --></i><span
                          style="margin-left: -10px"></span>
                      </div>
                      <div class="contact-row">
                        <i class="fa fa-envelope"></i>
                        <span style="margin-left: -10px">piyushpy@ivatle.com</span>
                        <!-- <i class="fa fa-envelope" style="margin-left: 20px"></i> --><span
                          style="margin-left: -10px"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 animate" data-animate="fadeInUp">
                    <form id="contactForm" method="post" novalidate="novalidate">
                      <div class="input-field">
                        <input type="text" class="form-control" name="name" id="name" required placeholder="Name" />
                      </div>
                      <div class="input-field">
                        <input type="email" class="form-control" name="email" id="email" required placeholder="Email" />
                      </div>
                      <div class="input-field">
                        <textarea class="form-control" name="message" id="message" required
                          placeholder="Message"></textarea>
                      </div>
                      <button class="btn" type="submit" id="submit_form">Submit</button>
                    </form>
                    <div id="form-messages" class="mt-3"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="section animated-row" data-section="Privacy policy">
                <div class="section-inner">
                    <div class="row justify-content-center">
                        <div class="col-md-7 wide-col-laptop">
                            <h1>hi</h1>
                        </div>
                    </div>
                </div>
            </div> -->
      <!-- <div class="section animated-row" >
                <div class="section-inner">
                    <div class="row justify-content-center">
                        <div class="col-md-7 wide-col-laptop">
                            <h1>hi</h1>
                        </div>
                    </div>
                </div>
            </div> -->
      <!-- <div id="social-icons">
            <div class="text-right">
                <ul class="social-icons">
                    <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#" title="Instagram"><i class="fa fa-behance"></i></a></li>
                </ul>
            </div>
        </div> -->
    </div> 

<?php include('includes/main_footer.php'); ?>
   


